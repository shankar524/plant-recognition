package com.shankar.leaf.util;

import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.core.Mat;

public class ImageFetcher {
	private String path="C:/Users/shank/OneDrive/Desktop/D/dark-leaf.jpg";
	
	public String getPath()
	{
		return this.path;
	}
	public void setPath(String path)
	{
		this.path=path;
	}
	public Mat getMatrixFromImage()
	{
		return Imgcodecs.imread(this.path);
	}
}
